const App = new Vue({
    el: '#app',
    data() {
        return {
            message: '',
            aTestimonials: [],
            aQuestions: [],
            oFormContact: {
                name_user: '',
                subject: '',
                email: '',
                message: '',
                bLoading: false
            }
        }
    },
    mounted() {
        this.loadTestimonials()
    },
    methods: {
        loadTestimonials() {
            axios.get(`http://localhost/seguros/admin/index.php?page=testimonial`)
                .then(res => {
                    const data = res.data;
                    this.aTestimonials = data.testimonials;
                    this.aQuestions = data.fquestions;
                })
                .catch((e) => {
                    console.log(e);
                })
        },
        insertContact() {
            this.oFormContact.bLoading = true;
            let isCompleted = false;
            for (const key in this.oFormContact) {
                if (key !== "bLoading" && this.oFormContact[key] != "") {
                    isCompleted = true;
                }
            }
            if (isCompleted) {
                axios.post(`http://localhost/seguros/admin/index.php?page=insert-contact`, this.oFormContact)
                    .then(res => {
                        if (res.data.message == "success") {
                            this.oFormContact = {
                                name_user: '',
                                subject: '',
                                email: '',
                                message: '',
                                bLoading: false
                            }
                            alert("Mensaje enviado")
                        }
                    })
                    .catch((e) => {
                        console.log(e);
                    })
            } else {
                alert("Todos los campos son obligatorios")
                this.oFormContact.bLoading = false;
            }
        }


    }
})