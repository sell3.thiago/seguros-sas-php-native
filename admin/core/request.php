<?php

require_once 'config.php';

$page = $_GET['page'];

if (!empty($page)) {
	$data = array(
		'login' 			 => array('model' => 'UsuarioModel', 	'view' => 'login', 		'controller' 		 => 'UsuarioController',),
		'inicio' 			 => array('model' => 'UsuarioModel',	'view' => 'inicio', 	'controller' 		 => 'InicioController'),
		'error' 			 => array('model' => 'UsuarioModel',	'view' => 'error', 		'controller' 		 => 'InicioController'),
		'testimonial'		 => array('model' => 'TestimonialModel', 'view' => 'getTestimonials', 'controller' 		 => 'TestimonialController'),
		'insert-contact'	 => array('model' => 'ContactModel', 	 'view' => 'insertContact', 'controller' 		 => 'ContactController', "parameters" => $_POST['info']),
	);

	foreach ($data as $key => $components) {
		if ($page == $key) {

			$model = $components['model'];
			$view = $components['view'];
			$parameters = $components['parameters'];
			$controller = $components['controller'];
			break;
		}
	}

	if (isset($model)) {
		try {
			require_once 'controllers/' . $controller . '.php';
			$objeto = new $controller();
			$objeto->$view();
		} catch (\Throwable $th) {
			echo "{$th->getMessage()} {$th->getLine()} {$th->getFile()}";
		}
	}
} else {
	header('Location: index.php?page=login');
}
