<?php
    require_once 'controllers/UsuarioController.php';
    $usuario = new UsuarioController();
    $usuario->login();
    if (isset($_POST['acceder'])) {
        $datos = array(
            'nick'    => $_POST['nick'],
            'password' => md5($_POST['password'])
        );
        $respuesta = $usuario->accesoUsuario($datos);
    }
?>
<div class="container-scroller">
	<div class="container-fluid page-body-wrapper full-page-wrapper">
		<div class="content-wrapper d-flex align-items-center auth px-0">
			<div class="row w-100 mx-0">
				<div class="col-lg-4 mx-auto">
					<div class="auth-form-light text-left py-5 px-4 px-sm-5">
						<div class="brand-logo mb-3">
							<img src="./template/images/logo.svg" alt="logo">
						</div>
						<div class="row">
							<div class="col">
								<?php
									if (isset($_GET['mensaje'])) {
										echo "<div class='alert alert-primary' role='alert'>".$_GET['mensaje']."</div>";
									} else {
										?>
										<h4>Hola bienvenido</h4>
										<h6 class="font-weight-light">Para continuar inicia sesión.</h6>
										<?php
									}
								?>
							</div>
						</div>
						<form action="index.php?page=login" method="POST" name="loginForm" id="loginForm">
							<div class="form-group">
								<label for="nick">Usuario</label>
								<input type="text" id="nick" name="nick" class="form-control"
									aria-describedby="nickHelp">
								<small id="nickHelp" class="form-text text-muted">Ingrese el usuario.</small>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" id="password" name="password" class="form-control"
									aria-describedby="passwordHelp">
								<small id="passwordHelp" class="form-text text-muted">Ingrese la contraseña</small>
							</div>
							<div class="mb-2">
								<button type="submit" name="acceder" class="btn btn-primary">Login</button>
								<a href="../" class="ml-3 btn btn btn-info">  Volver</a>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>