<?php

require_once 'controllers/PermisoController.php';
$objeto = new PermisoController();
$usuario_id = $_SESSION['id_usuario'];
$permisos = $objeto->obtenerPermisos($usuario_id);
?>
<div class="row" id="proBanner" style="display: none;">
	<div class="col-12">
		<span class="d-flex align-items-center purchase-popup">
			<p>Get tons of UI components, Plugins, multiple layouts, 20+ sample pages, and more!</p>
			<a href="https://www.bootstrapdash.com/product/celestial-admin-template/?utm_source=organic&utm_medium=banner&utm_campaign=free-preview" target="_blank" class="btn download-button purchase-button ml-auto">Upgrade To Pro</a>
			<i class="typcn typcn-delete-outline" id="bannerClose"></i>
		</span>
	</div>
</div>
<div class="container-fluid page-body-wrapper">
	<!-- partial:partials/_settings-panel.html -->
	<div class="theme-setting-wrapper">
		<div id="settings-trigger"><i class="typcn typcn-cog-outline"></i></div>
		<div id="theme-settings" class="settings-panel">
			<i class="settings-close typcn typcn-delete-outline"></i>
			<p class="settings-heading">SIDEBAR SKINS</p>
			<div class="sidebar-bg-options" id="sidebar-light-theme">
				<div class="img-ss rounded-circle bg-light border mr-3"></div>
				Light
			</div>
			<div class="sidebar-bg-options selected" id="sidebar-dark-theme">
				<div class="img-ss rounded-circle bg-dark border mr-3"></div>
				Dark
			</div>
			<p class="settings-heading mt-2">HEADER SKINS</p>
			<div class="color-tiles mx-0 px-4">
				<div class="tiles success"></div>
				<div class="tiles warning"></div>
				<div class="tiles danger"></div>
				<div class="tiles primary"></div>
				<div class="tiles info"></div>
				<div class="tiles dark"></div>
				<div class="tiles default border"></div>
			</div>
		</div>
	</div>
	<!-- partial -->
	<!-- partial:partials/_sidebar.html -->
	<nav class="sidebar sidebar-offcanvas" id="sidebar">
		<ul class="nav">
			<li class="nav-item">
				<div class="d-flex sidebar-profile">
					<div class="sidebar-profile-image">
						<img src="./template/images/faces/face29.png" alt="image">
						<span class="sidebar-status-indicator"></span>
					</div>
					<div class="sidebar-profile-name">
						<p class="sidebar-name">
							<?php echo $_SESSION['nombre']; ?>
						</p>
						<p class="sidebar-designation">
							Welcome
						</p>
					</div>
				</div>
				<p class="sidebar-menu-title">Menú de opciones</p>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/seguros/">
					<i class="typcn typcn-device-desktop menu-icon"></i>
					<span class="menu-title">Home</span>
					<!-- <span class="menu-title">Dashboard <span class="badge badge-primary ml-3">New</span></span> -->
				</a>
			</li>
			<!-- <li class="nav-item">
				<a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
					<i class="typcn typcn-briefcase menu-icon"></i>
					<span class="menu-title">UI Elements</span>
					<i class="typcn typcn-chevron-right menu-arrow"></i>
				</a>
				<div class="collapse" id="ui-basic">
					<ul class="nav flex-column sub-menu">
						<li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
						<li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">Dropdowns</a></li>
						<li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
					</ul>
				</div>
			</li> -->
		</ul>
	</nav>
	<!-- partial -->
	<div class="main-panel">
		<div class="content-wrapper">
			<div class="row">
				<div class="col-xl-12 d-flex grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<div class="d-flex flex-wrap justify-content-between">
								<h4 class="card-title mb-3">Device stats</h4>
							</div>
							<div class="row">
								<div class="col-12">
									<div class="row">
										<div class="col-sm-12">
											<div class="d-flex justify-content-between mb-4">
												<div>Uptime</div>
												<div class="text-muted">195 Days, 8 hours</div>
											</div>
											<div class="d-flex justify-content-between mb-4">
												<div>First Seen</div>
												<div class="text-muted">23 Sep 2019, 2.04PM</div>
											</div>
											<div class="d-flex justify-content-between mb-4">
												<div>Collected time</div>
												<div class="text-muted">23 Sep 2019, 2.04PM</div>
											</div>
											<div class="d-flex justify-content-between mb-4">
												<div>Memory space</div>
												<div class="text-muted">168.3GB</div>
											</div>
											<div class="progress progress-md mt-4">
												<div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- main-panel ends -->
</div>