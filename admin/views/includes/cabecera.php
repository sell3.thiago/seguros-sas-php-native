<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Seguros SAS</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- styles -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css/starter-template.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./template/vendors/typicons.font/font/typicons.css">
    <link rel="stylesheet" href="./template/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="./template/css/vertical-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="./template/images/favicon.png" />
    <style>
        .logout:hover {
            cursor: pointer !important;
        }
    </style>
</head>
<body>