<script src="./assets/js/jquery-3.4.1.slim.min.js"
	integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
	crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
	integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
	crossorigin="anonymous"></script>
<!-- template -->
<script src="./template/vendors/js/vendor.bundle.base.js"></script>
<script src="./template/js/off-canvas.js"></script>
<script src="./template/js/hoverable-collapse.js"></script>
<script src="./template/js/template.js"></script>
<script src="./template/js/settings.js"></script>
<script src="./template/js/todolist.js"></script>
<script src="./template/vendors/progressbar.js/progressbar.min.js"></script>
<script src="./template/vendors/chart.js/Chart.min.js"></script>
<script src="./template/js/dashboard.js"></script>
<script>
	$("#profileDropdown").on('click', function () {
		let menu = $(".dropdown-menu-right");
		if (menu.hasClass('show')) {
			menu.removeClass('show')
		} else {
			menu.addClass('show')
		}
	})
</script>
</body>

</html>