<?php
    require_once 'controllers/UsuarioController.php';
    $usuario = new UsuarioController();

    if (isset($_POST['salir'])) {
        $usuario->cerrarSesion();
    }
?>
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
		<a class="navbar-brand brand-logo" href="index.html"><img src="./template/images/logo.svg" alt="logo" /></a>
		<a class="navbar-brand brand-logo-mini" href="index.html"><img src="./template/images/logo-mini.svg"
				alt="logo" /></a>
		<button class="navbar-toggler navbar-toggler align-self-center d-none d-lg-flex" type="button"
			data-toggle="minimize">
			<span class="typcn typcn-th-menu"></span>
		</button>
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
		<ul class="navbar-nav navbar-nav-right">
			<li class="nav-item nav-profile dropdown">
				<a class="nav-link dropdown-toggle  pl-0 pr-0" href="#" data-toggle="dropdown" id="profileDropdown">
					<i class="typcn typcn-user-outline mr-0"></i>
					<span class="nav-profile-name">
						<?php echo $_SESSION['nombre']; ?>
					</span>
				</a>
				<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
					<a class="dropdown-item">
						<i class="typcn typcn-cog text-primary"></i>
						Settings
					</a>
					<a class="dropdown-item logout">
						<form action="" method="POST" name="logoutForm" id="logoutForm">
							<button name="salir" type="submit" class="btn btn-sm">
								<i class="typcn typcn-power text-primary"></i>
								Salir
							</button>
						</form>
					</a>
				</div>
			</li>
		</ul>
		<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
			data-toggle="offcanvas">
			<span class="typcn typcn-th-menu"></span>
		</button>
	</div>
</nav>

<!-- <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	<a class="navbar-brand" href="#">
		<?php echo NOMBRE_SISTEMA; ?>
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
		aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="index.php?page=inicio">Inicio <span class="sr-only">(current)</span></a>
			</li>
		</ul>
		<ul class="navbar-nav text-right">
			<li class="nav-item"><a class="nav-link" href="#">
					<?php echo $_SESSION['nombre']; ?>
				</a></li>
			<li class="nav-item">
				<form action="" method="POST" name="logoutForm" id="logoutForm">
					<button type="submit" class="btn btn-link nav-link" name="salir">Cerrar sesión</button>
				</form>
			</li>
		</ul>
	</div>
</nav> -->