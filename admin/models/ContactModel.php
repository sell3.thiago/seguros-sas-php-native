<?php
require_once 'ModeloBase.php';

class ContactModel extends ModeloBase
{

    public function __construct()
    {
        parent::__construct();
    }

    public function insertContact($request)
    {
        $db = new ModeloBase();
        $sth = $db->insert($request);
        return $sth;
    }

    public function getContactRegisters()
    {
        $db = new ModeloBase();
        $query = "SELECT t.*
		FROM form_contact AS t";
        $sth = $db->getAll($query);
        return $sth;
    }
}
