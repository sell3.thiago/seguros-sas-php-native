/*
 Sistema Registro de Productos -  Catálogo
 */
DROP DATABASE IF EXISTS seguros;

CREATE DATABASE IF NOT EXISTS seguros;

USE seguros;

/*tabla usuario*/
CREATE TABLE usuario(
    id_usuario INT(4) PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    nick VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL
) ENGINE = InnoDB;

-- tabla testimonios
CREATE TABLE `seguros`.`testimonials` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name_user` VARCHAR(200) NOT NULL,
    `title_job_user` VARCHAR(200) NOT NULL,
    `comment` VARCHAR(2000) NOT NULL,
    `image` VARCHAR(200) NOT NULL PRIMARY KEY (`id`)
) ENGINE = InnoDB;

-- galeria
CREATE TABLE `gallery` (
    `id` int(11) NOT NULL,
    `title` varchar(500) NOT NULL,
    `path` varchar(2000) NOT NULL,
    `observations` varchar(2000) NOT NULL,
    `isDashboard` tinyint(4) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

-- preguntas frecuentes
CREATE TABLE `seguros`.`frequent_questions` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `question` VARCHAR(1000) NOT NULL,
    `answer` VARCHAR(2000) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `counter_likes` BIGINT(10) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `seguros`.`form_contact` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name_user` VARCHAR(200) NOT NULL,
    `email` VARCHAR(200) NOT NULL,
    `subject` VARCHAR(200) NOT NULL,
    `message` TEXT NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

/*insertar datos*/
INSERT INTO
    `usuario` (`id_usuario`, `nombre`, `nick`, `password`)
VALUES
    (
        1,
        'Admin',
        'admin',
        '21232f297a57a5a743894a0e4a801fc3'
    );