<?php

require_once './libs/DB.php';

class ModeloBase extends DB {
	public $db;
	public $string;

	public function __construct() {
		$this->db = new DB();
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}

	public function consultarRegistro($query) {
		try {
			$consulta = $this->db->query($query);
			if ($consulta->rowCount() == 1) {
				return $consulta;
			} else {
				return false;
			}
		} catch (PDOException $e){
			echo "Error: ".$e->getMessage();
		}
	}

	public function obtenerTodos($query) {
		try {
			return $this->db->query($query);
		} catch (PDOException $e){
			echo "Error: ".$e->getMessage();
		}
	}

	public function insert($request) {
		$this->db->beginTransaction();
		try {
			$data = [
				'name_user' => $request->name_user,
				'email'		=> $request->email,
				'subj' 	=> $request->subject,
				'messa' 	=> $request->message,
			];
			$sql = "INSERT INTO form_contact VALUES (NULL, :name_user, :email, :subj, :messa, CURRENT_TIMESTAMP())";
			$stmt= $this->db->prepare($sql);
			$stmt->execute($data);
			$this->db->commit();
			$response = new stdClass();
			$response->message = 'success';
			return $response;
		} catch (PDOException $e){
			$this->db->rollBack();
			echo "Error: ".$e->getMessage();
		}
	}

	public function getAll($query) {
		try {
			$sth = $this->db->prepare($query);
			$sth->execute();
			return $sth->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e){
			echo "Error: ".$e->getMessage();
		}
	}

}
