<?php
require_once 'ModeloBase.php';

class FQuestionModel extends ModeloBase
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getQuestions()
    {
        $db = new ModeloBase();
        $query = "SELECT t.*
		FROM frequent_questions AS t";

        
        $sth = $db->getAll($query);
        return $sth;
    }
}
