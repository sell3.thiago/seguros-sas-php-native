<?php
require_once 'ModeloBase.php';

class TestimonialModel extends ModeloBase
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getTestimonials()
    {
        $db = new ModeloBase();
        $query = "SELECT t.*
		FROM testimonials AS t";

        
        $sth = $db->getAll($query);
        return $sth;
    }
}
