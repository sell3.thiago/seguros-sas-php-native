<?php
require_once('models/TestimonialModel.php');
require_once('models/FQuestionModel.php');

class TestimonialController
{
    public function getTestimonials()
    {
        $testimonial = new TestimonialModel();
        $FQuestion   = new FQuestionModel();
        $response = new stdClass();
        $response->testimonials = $testimonial->getTestimonials();
        $response->fquestions   = $FQuestion->getQuestions();
        echo json_encode($response);
    }
}
