<?php
require_once('models/ContactModel.php');

class ContactController
{
    public function insertContact()
    {
        $request_body = file_get_contents('php://input');
        $request = json_decode($request_body);
        $contact = new ContactModel();
        $res = $contact->insertContact($request);
        echo json_encode($res);
    }
}
